# -*- coding: utf-8 -*-
"""
Created on Fri May 25 16:21:07 2018

@author: miquel.sarrias
"""

##• pag 12/03/2018 16/04/2018


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time
from PIL import Image, ImageDraw
import random



def cami(matr_lab, entrada, num, img):
    movs=[-1,0,1]
    fila=entrada[0]
    columna=entrada[1]
    count1=0
    cont=True
    while cont:
        count1+=1
        matr_lab[fila][columna]=num
        nook=True
        count2=0
        while nook:
            count2+=1
            f=random.choice(movs)
            c=random.choice(movs)
    
            
            if fila+f < 0 or fila+f >= len(matr_lab) or columna+c < 0 or columna+c >= len(matr_lab):
                nook = True
            elif matr_lab[fila+f][columna+c] == 0:
                nook = False
                if np.abs(f)+np.abs(c) >1:
                    nook = True
            if nook == False:
                fila+=f
                columna+=c
                num+=1
                if f==1:
                    img_cela=Image.new(mode='RGB', size=(3,5), color=(255,255,255))
                    img.paste(img_cela,(columna*5+2,fila*5+2-2))
                if f==-1:
                    img_cela=Image.new(mode='RGB', size=(3,5), color=(255,255,255))
                    img.paste(img_cela,(columna*5+2,fila*5+2))
                if c==1:
                    img_cela=Image.new(mode='RGB', size=(5,3), color=(255,255,255))
                    img.paste(img_cela,(columna*5+2-2,fila*5+2))
                if c==-1:
                    img_cela=Image.new(mode='RGB', size=(5,3), color=(255,255,255))
                    img.paste(img_cela,(columna*5+2,fila*5+2))
            if count2>10:
                matr_lab[fila][columna]=num
                break
        if count1>50:
            matr_lab[fila][columna]=num
            break
    return (matr_lab, num, img)



def resolucióLab(entrada, sortida, img):
    img2=img
    pixels=img2.load()

    cont=True
    entrada_esq=[entrada[1]*5+2,entrada[0]*5+2]
    columna=entrada_esq[0]
    fila=entrada_esq[1]
    direccio=1
    
    while cont:
        pixels[columna,fila]=(255,0,0)
          
        if direccio==0:
            if sum(pixels[columna,fila-1]) != 0:
                direccio=1
                fila=fila-1
            elif sum(pixels[columna+1,fila]) != 0:
                direccio=0
                columna=columna+1
            else:
                direccio=3
                fila=fila+1
    
        elif direccio==1:
            if sum(pixels[columna-1,fila]) != 0:
                direccio=2
                columna=columna-1
            elif sum(pixels[columna,fila-1]) != 0:
                direccio=1
                fila=fila-1
            else:
                direccio=0
                columna+=1
                
        elif direccio==2:
            if sum(pixels[columna,fila+1]) != 0:
                direccio=3
                fila=fila+1
            elif sum(pixels[columna-1,fila]) != 0:
                direccio=2
                columna=columna-1
            else:
                direccio=1
                fila=fila-1
    
        elif direccio==3:
            if sum(pixels[columna+1,fila]) != 0:
                direccio=0
                columna=columna+1
            elif sum(pixels[columna,fila+1]) != 0:
                direccio=3
                fila=fila+1
            else:
                direccio=2
                columna=columna-1
    
        if columna==sortida[1]*5-1 and fila==sortida[0]*5-1:
            cont=False

    cont=True
    sortida_dre=[entrada[1]*5+2,entrada[0]*5+4]
    columna=sortida_dre[0]
    fila=sortida_dre[1]
    direccio=3
    
    while cont:
    
        pixels[columna,fila]=(0,255,0)
        if direccio==0:
            if sum(pixels[columna,fila+1]) != 0:
                direccio=3
                fila=fila+1
            elif sum(pixels[columna+1,fila]) != 0:
                direccio=0
                columna=columna+1
            else:
                direccio=1
                fila=fila-1
    
        elif direccio==1:
            if sum(pixels[columna+1,fila]) != 0:
                direccio=0
                columna=columna+1
            elif sum(pixels[columna,fila-1]) != 0:
                direccio=1
                fila=fila-1
            else:
                direccio=2
                columna-=1
                
        elif direccio==2:
            if sum(pixels[columna,fila-1]) != 0:
                direccio=1
                fila=fila-1
            elif sum(pixels[columna-1,fila]) != 0:
                direccio=2
                columna=columna-1
            else:
                direccio=3
                fila=fila+1
    
        elif direccio==3:
            if sum(pixels[columna-1,fila]) != 0:
                direccio=2
                columna=columna-1
            elif sum(pixels[columna,fila+1]) != 0:
                direccio=3
                fila=fila+1
            else:
                direccio=0
                columna=columna+1
    
        if columna==sortida[1]*5-1 and fila==sortida[0]*5-1: 
            cont=False
    
    len_cami=0
    for columna in range(1,(lenmatr)*5+2-1):
        for fila in range(1,(lenmatr)*5+2-1):
            pix_veins=[pixels[columna-1,fila-1],pixels[columna-1,fila+1],pixels[columna+1,fila-1],pixels[columna+1,fila+1]]
            if ((255,0,0) in pix_veins) and ((0,255,0) in pix_veins):
                pixels[columna,fila]=(0,0,255)
                len_cami+=1
    
    for columna in range(1,(lenmatr)*5+2-1):
        for fila in range(1,(lenmatr)*5+2-1):
            if pixels[columna,fila]==(255,0,0) or pixels[columna,fila]==(0,255,0):
                pixels[columna,fila]=(255,255,255)
                
    
    return (img2,len_cami)
                

lenmax=0 #contador
for i in range(3):
    lenmatr=50
    matr_lab = [[0]*lenmatr for x in range(lenmatr)]
    entrada = [0,0]
    sortida = [lenmatr,lenmatr]
    num=1
    print('entrada (fila col):',entrada)
    
    img=Image.new(mode='RGB', size=((lenmatr)*5+2,(lenmatr)*5+2), color=(0,0,0))
    matr_lab,num,img=cami(matr_lab,entrada,num,img)
    
    desvios=0
    for i in range(100):
    #    print(i)
        matr_des = [[0]*lenmatr for x in range(lenmatr)]
        for fila in range(1,len(matr_lab)-1):
            for columna in range(1,len(matr_lab)-1):
                if matr_lab[fila][columna] != 0:
                    opc=[0]*3+[1]
                    if random.choice(opc) == 1:
                        matr_des[fila][columna] = -1
        
        for fila in range(len(matr_lab)):
            for columna in range(len(matr_lab)):
                if matr_des[fila][columna] == -1:
                    if matr_lab[fila-1][columna] == 0 or matr_lab[fila+1][columna] == 0 or matr_lab[fila][columna-1] == 0 or matr_lab[fila][columna+1] == 0:
                        desvios+=1
                        matr_lab,num,img=cami(matr_lab,[fila, columna],num,img)
    #                    img.save('Lab_pix_'+str(desvios)+'.png')
     
    img_cela=Image.new(mode='RGB', size=(3,3), color=(255,40,200))
    img.paste(img_cela,(entrada[1]*5+2,entrada[0]*5+2))
    img.paste(img_cela,(sortida[1]*5-3,sortida[0]*5-3))
        
    matr=pd.DataFrame(matr_lab)
           
    #imgplot = plt.imshow(img)
    #imgplot.axes.get_xaxis().set_visible(False)
    #imgplot.axes.get_yaxis().set_visible(False)
    #imgplot.axes.set_frame_on(False)
    #img.save('Lab_'+str(np.random.uniform(0,1))+'.png')
    
    
    print('Lab generat')
    
    

    img2,len_cami=resolucióLab(entrada, sortida, img)
    if len_cami > lenmax:
        print(len_cami)
        img.save('Lab_pix.png')
        img2.save('Lab_pix_res.png')
        lenmax=len_cami
    
    print('Lab resolt, camí solució de',len_cami,'celes.')
    print('('+str(len_cami/(lenmatr*10))+') vegades el mínim')
    
    


































